<?php

//ホームページアドレス
define('URL', "http://namaste-in.com/");
//ホームページタイプ
define('EXT', "htm|html");
//file 1=昇 0=降
define('SORT', "1");
//除外ページ
define('NONE', "0");
//取得するファイル名
define('ONLYL', "0");
//取得開始ファイル名
define('ONLYM', "index.html");
//日付(1=ファイルの更新日時 2=常に最新日時 0=指定しない)
define('DAY', "2");
//更新頻度(全フォルダ共通(always,hourlyなど) or 0=指定しない)
define('CHA', "weekly");
//優先度(全フォルダ共通 1=指定する 0=指定しない)
define('PRI0', "0");
//ルートフォルダの優先度(0.0から1.0まで)
define('PRI', "1.0");
//サブフォルダ(最後の/なし)と優先度(0.0から1.0まで)
$dirs = array(
"breakthrough" => "0.8",
);

header('Content-Type:text/xml; charset=utf-8' );

$dir_path = getcwd();
$filename = array();
$all = 0;

//ルートフォルダ
$dir = dir($dir_path);
while($file = $dir->read()) {
	if(!ereg('(^\.$)|(^\.\.$)', $file)) {
		$filename[] = $file;
		rsort($filename); //降順
		if(SORT){
			sort($filename); //昇順
		}
	}
}
$dir->close();
for($i=0;$i<count($filename);$i++) {
	if(ereg(EXT,$filename[$i])){
		if(NONE){if(ereg(NONE,$filename[$i])){continue;}}
		if(ONLYL){if(!ereg(ONLYL,$filename[$i])){continue;}}
		if(DAY==2){$ltime = date("Y-m-d\TH:i:s+09:00");}
		if(DAY==1){$mtime = filemtime("$filename[$i]");
		$ltime = date("Y-m-d\TH:i:s+09:00" ,$mtime);}
		$filenamet.= '<url>
<loc>'.URL.''.$filename[$i].'</loc>';
		if(DAY){$filenamet.= '
<lastmod>'.$ltime.'</lastmod>';}
		if(CHA){$filenamet.= '
<changefreq>'.CHA.'</changefreq>';}
		if(PRI0){$filenamet.= '
<priority>'.PRI.'</priority>';}
		$filenamet.= '
</url>
';
	}
	if(ereg("index.",$filename[$i])){
		if(NONE){if(ereg(NONE,$filename[$i])){continue;}}
		if(ONLYL){if(!ereg(ONLYL,$filename[$i])){continue;}}
		if(DAY==2){$ltime = date("Y-m-d\TH:i:s+09:00");}
		if(DAY==1){$mtime = filemtime("$filename[$i]");
		$ltime = date("Y-m-d\TH:i:s+09:00" ,$mtime);}
		$filenamei.= '<url>
<loc>'.URL.'</loc>';
		if(DAY){$filenamei.= '
<lastmod>'.$ltime.'</lastmod>';}
		if(CHA){$filenamei.= '
<changefreq>'.CHA.'</changefreq>';}
		if(PRI0){$filenamei.= '
<priority>'.PRI.'</priority>';}
		$filenamei.= '
</url>
';
	}
}

//サブフォルダ
while(list ($key, $val) = each($dirs)){
	if (is_dir($key)) {
		if ($dh = opendir($key)) {
			$pri = "$val";
			while (($files = readdir($dh)) !== false) {
				$file[] = $files;
			}
			rsort($file); //降順
			if(SORT){
				sort($file); //昇順
			}
			$all = 0;
			for($i=0;$i<count($file);$i++) {
				if(ereg("index.",$file[$i])){
					if(NONE){if(ereg(NONE,$file[$i])){continue;}}
					if(ONLYM){if(!ereg(ONLYM,$file[$i])){continue;}}
					if(DAY==2){$ltime = date("Y-m-d\TH:i:s+09:00");}
					if(DAY==1){$mtime = filemtime("$key/$file[$i]");
					$ltime = date("Y-m-d\TH:i:s+09:00" ,$mtime);}
					$filenames.= '<url>
<loc>'.URL.''.$key.'/</loc>';
					if(DAY){$filenames.= '
<lastmod>'.$ltime.'</lastmod>';}
					if(CHA){$filenames.= '
<changefreq>'.CHA.'</changefreq>';}
					if(PRI0){$filenames.= '
<priority>'.$pri.'</priority>';}
					$filenames.= '
</url>
';
				}
				if(ereg(EXT,$file[$i])){
					if(NONE){if(ereg(NONE,$file[$i])){continue;}}
					if(ONLYM){if(!ereg(ONLYM,$file[$i])){continue;}}
					if(DAY==2){$ltime = date("Y-m-d\TH:i:s+09:00");}
					if(DAY==1){$mtime = filemtime("$key/$file[$i]");
					$ltime = date("Y-m-d\TH:i:s+09:00" ,$mtime);}
					$filenames.= '<url>
<loc>'.URL.''.$key.'/'.$file[$i].'</loc>';
					if(DAY){$filenames.= '
<lastmod>'.$ltime.'</lastmod>';}
					if(CHA){$filenames.= '
<changefreq>'.CHA.'</changefreq>';}
					if(PRI0){$filenames.= '
<priority>'.$pri.'</priority>';}
					$filenames.= '
</url>
';
				}
				$all++;
			}
			closedir($dh);
			$file = array();
		}
	}
}

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
'.$filenamei.''.$filenamet.''.$filenames.'</urlset>';

echo $xml;

?>
