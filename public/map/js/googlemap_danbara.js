//<![CDATA[
var svObj=null;
var svContainer,marker, infoWnd;

function load(){
	var container = document.getElementById("map");
	var map = new GMap2(container);
	//地図の表示/////////////////////////////////////////////////////////
    var point=new GLatLng(34.384543,132.477543);
    map.setCenter(point, 17);
    //マップコントローラを付ける
    map.addControl(new GLargeMapControl());
	// ＡＩＣデンタルクリニック//////////////////////////////////////////
	var latlng01 = new GLatLng(34.384543,132.477543);
	// アイコンオプションの設定
	var icon01 = new GIcon();
	// アイコン画像のURL
	icon01.image = "http://www.namaste-in.com/map/images/googlemap_icon.gif";
	// アイコン画像のサイズ（横幅/縦幅）
	icon01.iconSize = new GSize(64, 64);
	// アイコンの表示場所
	icon01.iconAnchor = new GLatLng(64, 32);
	// マーカーの表示
	map.addOverlay( new GMarker(latlng01, icon01) ); 
    //ストリートビューの初期化////////////////////////////////////////////
    svContainer = document.createElement("div");
    svContainer.style.width="250px";
    svContainer.style.height="200px";
	//GStreetviewOverlayの追加
    map.addOverlay(new GStreetviewOverlay());
    //マーカーを作成
    marker = new GMarker(point,{draggable: true});
    //ふきだしを表示する
    GEvent.addListener(marker, "click", showStreetView);
    //マーカーがドラッグを開始したら、ふきだしを閉じる
    GEvent.addListener(marker, "dragstart", function(){
    	map.closeInfoWindow();
    			if(!svObj && svObj!=undefined){
            		svObj.remove();
                }
            });
            GEvent.addListener(marker, "dragend", function(){
                GEvent.trigger(marker, "click");
            });
            map.addOverlay(marker);
            //マーカーのふきだしを開く
            marker.openInfoWindowHtml("<center><b><font color=green>NAMASTE DANBARA</font></b><br>INDIAN&NEPALI RESTAURANTF<br>TEL:082-262-0004</center>");
	}
    
    function showStreetView(){
    //ストリートビューの表示////////////////////////////////////////////
            var markerPos = marker.getPoint();
            svObj = new GStreetviewPanorama(svContainer,{latlng:markerPos});
            //ふきだしの表示
            marker.openInfoWindow(svContainer);
    }
    GEvent.addDomListener(window, "load", load);
    GEvent.addDomListener(window, "unload", GUnload);
	
//]]>